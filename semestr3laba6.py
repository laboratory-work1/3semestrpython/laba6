import random
import timeit
import matplotlib.pyplot as plt

def partition(nums, low, high):
    """Вспомогательная функция для быстрой сортировки"""
    pivot = nums[(low + high) // 2]
    i = low - 1
    j = high + 1
    while True:
        i += 1
        while nums[i] < pivot:
            i += 1
        j -= 1
        while nums[j] > pivot:
            j -= 1
        if i >= j:
            return j
        nums[i], nums[j] = nums[j], nums[i]

def quick_sort(nums):
    """Быстрая сортировка.

    На вход подается массив чисел.
    """
    def _quick_sort(items, low, high):
        if low < high:
            split_index = partition(items, low, high)
            _quick_sort(items, low, split_index)
            _quick_sort(items, split_index + 1, high)
    _quick_sort(nums, 0, len(nums) - 1)


def bubble_sort(nums):
    """Сортировка пузырьком.

    На вход подается массив чисел.
    """
    swapped = True
    while swapped:
        swapped = False
        for i in range(len(nums) - 1):
            if nums[i] > nums[i + 1]:
                nums[i], nums[i + 1] = nums[i + 1], nums[i]
                swapped = True

def insertion_sort(nums):
    """Сортировка вставками.

    На вход подается массив чисел.
    """
    for i in range(1, len(nums)):
        item_to_insert = nums[i]
        j = i - 1
        while j >= 0 and nums[j] > item_to_insert:
            nums[j + 1] = nums[j]
            j -= 1
        nums[j + 1] = item_to_insert

def shell_sort(nums):
    """Сортировка Шелла.

    На вход подается массив чисел.
    """
    inc = len(nums) // 2
    while inc:
        for i, el in enumerate(nums):
            while i >= inc and nums[i - inc] > el:
                nums[i] = nums[i - inc]
                i -= inc
            nums[i] = el
        inc = 1 if inc == 2 else int(inc * 5.0 / 11)

def counting_time(sort):
    """Функция подсчета времени выполнения функций сортировок.

    На вход подается название функции сортировки.
    На выходе получается 3 словаря с данными о размерах массивов и времени.
    """
    timerand = dict()
    timeinc = dict()
    timedec = dict()
    for n in range(100000, 1000000 + 1, 100000): # тут задаются размер и шаг массивов
        # по условию задачи размер массива 100к-1М элементов с шагом 100к элементов

        rand = [random.randint(0, 1000) for i in range(n)]
        inc = [random.randint(0, 1000) for i in range(n)]
        inc.sort()
        dec = [random.randint(0, 1000) for i in range(n)]
        dec.sort(reverse=True)

        start_time_rand = timeit.default_timer();
        sort(rand)
        timerand[n] = timeit.default_timer() - start_time_rand

        start_time_inc = timeit.default_timer();
        sort(inc)
        timeinc[n] = timeit.default_timer() - start_time_inc

        start_time_dec = timeit.default_timer();
        sort(dec)
        timedec[n] = timeit.default_timer() - start_time_dec

    return timerand, timeinc, timedec


plt.figure(figsize=(12, 7)) #размер общего окна графиков
q = 1 #счетчик для последовательности окон графиков и их заголовков

for sort in [bubble_sort, insertion_sort, quick_sort, shell_sort]:

    timerand, timeinc, timedec = counting_time(sort)

    plt.subplot(2,2,q)
    plt.grid(True)
    plt.plot(timerand.keys(), timerand.values(), label = 'random')
    plt.plot(timeinc.keys(), timeinc.values(), label = 'increase')
    plt.plot(timedec.keys(), timedec.values(), label = 'decrease')
    listnamesort = [' ', 'Сортировка пузырьком', 'Сортировка вставками', 'Быстрая сортировка', 'Сортировка Шелла']
    namesort = listnamesort[q]
    plt.title('{}'.format(namesort))
    plt.ylabel('секунды')
    plt.legend()
    q += 1

plt.show()
